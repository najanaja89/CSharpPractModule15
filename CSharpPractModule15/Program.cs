﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CSharpPractModule15
{
    class Program
    {
        static void Main(string[] args)
        {
            string Data = "One morning, as Gregor Samsa was waking up from anxious dreams, he discovered that in bed he had been changed into a monstrous verminous bug. ";
            Type type = typeof(string);

            foreach (var method in type.GetMethods())
            {
                if (method.Name.Contains("Substring") && (method.GetParameters().Count() == 2))
                {
                    Console.WriteLine((method as MethodInfo).Invoke(Data,
                        new object[] { 6, 15 }));
                }

            }
            Console.WriteLine("-----------------------------");
            Type typeList = typeof(List<>);

            Console.WriteLine("List constructors: ");
            foreach (var member in typeList.GetConstructors())
            {
                Console.WriteLine(member);
            }

            Console.ReadLine();
        }
    }
}
